from math import sqrt


def triangle_area(a, b, c):
    p = (a+b+c)/2
    return round(sqrt(p*(p-a)*(p-b)*(p-c)), 2)


if __name__ == '__main__':
    print(triangle_area(4.5, 5.9, 9))
