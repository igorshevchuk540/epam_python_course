def sort_file(input_file: str, output_file: str):
    with open(input_file, 'r') as inp:
        with open(output_file, 'w') as out:
            for line in sorted(inp):
                # print(line, end='')
                out.write(line)


if __name__ == '__main__':
    in_file = 'unsorted_names.txt'
    out_file = 'sorted_names.txt'
    sort_file(in_file, out_file)
