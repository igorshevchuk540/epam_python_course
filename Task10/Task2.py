import string


def test_trans(s):
    return s.translate(str.maketrans('', '', string.punctuation))


def get_common_words(input_file: str, amount: int = 2):
    d = {}
    with open(input_file, 'r') as inp:
        text = test_trans(inp.read()).lower()
        for word in text.split(' '):
            if word not in d.keys():
                d[word] = 1
            else:
                d[word] += 1
    sorted_dict = {k: v for k, v in sorted(d.items(), key=lambda item: item[1], reverse=True)}
    result = iter(sorted_dict)
    for i in range(amount):
        r = result.__next__()
        print(r, d[r])


if __name__ == '__main__':
    in_file = 'lorem_ipsum.txt'
    get_common_words(in_file, amount=3)
