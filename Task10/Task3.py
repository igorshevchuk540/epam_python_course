import pandas


def get_top_performers(file_path: str, number_of_top_students=5):
    data = pandas.read_csv(file_path)
    return data.sort_values('average mark', ascending=False)['student name'].head(number_of_top_students)


def write_desc_age(file_path: str):
    data = pandas.read_csv(file_path)
    data.sort_values('age', ascending=True).to_csv(r'sorted_students.csv')
    return 'Data was written to file'


if __name__ == '__main__':
    file = 'students.csv'
    print(get_top_performers(file, 4))
    print(write_desc_age(file))
