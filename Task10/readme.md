# Home task for week 10

[Task 1](https://gitlab.com/igorshevchuk540/epam_python_course/-/blob/master/Task10/Task1.py)<br>
`Task1.py` has a function *sort_file*, that takes file with celebrities names, and sorts them.<br>




[Task 2](https://gitlab.com/igorshevchuk540/epam_python_course/-/blob/master/Task10/Task2.py)<br>
`Task2.py` has a function *get_common_words*, that takes a text file, and int number, and returns the right amount of most common words in this file.<br>



[Task 3](https://gitlab.com/igorshevchuk540/epam_python_course/-/blob/master/Task10/Task3.py)<br>
`Task3.py`has two functions:
1. Takes `.csv` file and returns top 5 performers, sorted by average mark.
2. Takes `.csv` file and writes data to another csv file, sorted by age by ascending.



