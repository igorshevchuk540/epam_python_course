results = [None]


def remember_result(func):
    def wrapper(*args):
        global results
        print(f"Last result = '{results[0]}'")
        results.append(func(*args))
        del results[0]
    return wrapper


@remember_result
def sum_list(*args):
    result = ""
    for item in args:
        result += str(item)
    print(f"Current result = '{result}'")
    return result


if __name__ == '__main__':
    sum_list('a', 'b')
    sum_list("abc", "cde")
    sum_list(3, 4, 5)
    sum_list(1, 2)
    sum_list(5, 7)
    sum_list(23, 39)

