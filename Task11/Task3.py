results = []


def call_once(func):
    def wrapper(*args):
        global results
        results.append(func(*args))
        print(f"Sum of elements '{results[0]}'")
    return wrapper


@call_once
def sum_of_numbers(a, b):
    return a + b


if __name__ == '__main__':
    sum_of_numbers(13, 42)
    sum_of_numbers(999, 100)
    sum_of_numbers(134, 412)
    sum_of_numbers(856, 232)
