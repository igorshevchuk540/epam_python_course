# Home task for week 11

[Task 1](https://gitlab.com/igorshevchuk540/epam_python_course/-/blob/master/Task11/Task1.py)<br>
The original code is below
```python
a = "I am global variable!"


def enclosing_funcion():
    a = "I am variable from enclosed function!"

    def inner_function():
        a = "I am local variable!"
        print(a)
```
To complete **Task1.1** we have to add line `return inner_function()` in the `enclosing function`<br><br>

Code for **Task1.2**:
````python
a = "I am global variable!"


def enclosing_funcion():
    a = "I am variable from enclosed function!"

    def inner_function():
        global a
        #a = "I am local variable!"
        print(a)
    return inner_function()
````
Code for **Task1.3**:
````python
a = "I am global variable!"


def enclosing_funcion():
    # global a
    a = "I am variable from enclosed function!"

    def inner_function():
        nonlocal a
        # a = "I am local variable!"
        print(a)
    return inner_function()
````


[Task 2](https://gitlab.com/igorshevchuk540/epam_python_course/-/blob/master/Task11/Task2.py)<br>
In `Task2.py` i had to create decorator function `remember_result`, that can store previous result of function call.<br>
For this purpose i used list, in which i store function calls.<br><br>

[Task 3](https://gitlab.com/igorshevchuk540/epam_python_course/-/blob/master/Task11/Task3.py)<br>
`Task3.py`
In `Task2.py` i had to create decorator function `call_once`, that will store first result of function call.<br>
Call a function with this decorator will always return the same result.
For this purpose I also used list, as in `Task2.py` in which i store function calls.<br><br>



<br><br>
`Task4`<br>
Statement: `Run the module modules/mod_a.py. Check its result. Explain why
does this happen. Try to change x to a list [1,2,3]. Explain the
result. Try to change import to from x import * where x - module
names. Explain the result.`

At first run the following code:
````python
#mod_a code
from Task11.Packages import mod_c
print(mod_c.x)
````
The result is 5, because `mod_c` contains value *x=5*.<br>

After that i changed code to:
````python
#mod_a code
from Task11.Packages import mod_b

print(mod_b.mod_c.x)
````
and run it. Result stays the same, we just call value x, from `mod_b`, that imports `mod_c`.

After that i changed code of file `mod_c` to:
````python
#mod_c code
x = [1, 2, 3]
````
Result also stays the same, because we set  value *x* to 5 in module `mod_b`.

We can also use this import to use value x:
````python
from Task11.Packages.mod_b import *
print(mod_c.x)
````
This construction prints 5 as well.

Conclusion to `Task4`:
In file `mod_b.py` does not have it\`s own valus, so we always set number to value x from `mod_c`.