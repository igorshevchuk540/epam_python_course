# Home task for week 12

In homework for this week we had to install and learn basics about **Linux**. For this purpose i installed WSL(Windows Subsystem for Linux) according to Microsoft`s tutorial.
Also i installed Linux terminal for Windows.

`Task 1`<br>
First command is ls. It has a lot of different arguments, so i had to check manual below.
![Photo0](ls-help.png)
We start with checking our current folder. To do this we use command `ls` with parameters:
1. -l
2. -a
3. -la
4. -lda
<br>

![Photo1](1.png)
![Photo2](2.png)
``mkdir`` creates folder `test`;

`cd` - abbreviation to change directory;

`pwd` displays current directory;

``touch`` creates new file;

``mv`` moves file to different place;

``cp`` copies file;

``rm`` and ``rmdir`` deletes a specific file or directory;
![Photo3](3.png)
![Photo4](4.png)

In next photo we can see the difference between 3 commands:
1. `cat` displays file\`s contents.
2. `less` shows end of the file in real time, so we can track log files in that way. 
3. `more` displays one page of file(in case it is huge). Also used in logs
![Photo5](5.png)


`Task 2a`<br>
In this task i was discovering soft and hard links. 
Terminal commands are provided in the screenshot below.

The difference between *soft* and *hard* link is that *soft link* is an actual link to the original file.
If we delete the original file, the soft link has no value, because it points to non-existent file.

*Hard link* is the mirror copy of file. That means that if you delete the original file, the hard link will still has the data of the original file.
![Photo6](6.png)


`Task 2b`<br>
In this task we will create a simple script, that echos a message to terminal.
To do this we create file with extension *sh* and make it runnable with command `chmod+x`
After that we simply execute it with command `sh script.sh`
![Photo7](7.png)
![Photo8](8.png)

`Conclusion`:
In this task I learned a lot about Linux, terminal. Tested a lot of commands, and created my own script.
