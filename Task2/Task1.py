import re


def count_negatives(numbers: list) -> int:
    return len(re.findall(r'-[0-9]+', ' '.join(map(str, numbers))))
    # return str(numbers).count('-')


if __name__ == '__main__':
    input_numbers = [4, -9, 8, -11, 8]
    print(f'There are {count_negatives(input_numbers)} negative numbers in list {input_numbers}')


# b = ' '.join(map(str, a))
# r = '-[0-9]+'
# res = re.findall(r'-[0-9]+', b)
# print(res)
# print(len(res))
