def change_words(string: str):
    # Solution 1
    arr = string.split(' ')
    reas = arr.index('reasonable')
    unreas = arr.index('unreasonable')
    arr[reas], arr[unreas] = arr[unreas], arr[reas]
    print(' '.join(arr))
    # Solution 2
    # s = string.split(' ')
    # s[1], s[-11] = s[-11], s[1]
    # print(' '.join(s))


if __name__ == '__main__':
    change_words(
        string='The reasonable man adapts himself to the world; the unreasonable one persists in trying to adapt the '
               'world to himself'
    )
