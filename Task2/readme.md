# Home task for week 2

[Task 1](https://gitlab.com/igorshevchuk540/epam_python_course/-/blob/master/Task2/Task1.py)<br>
`Task1.py` counts amount of negative numbers in list without using conditionals or loops.<br>
File provides two solutions:
* Using regular expressions:
```python
def count_negatives(numbers: list) -> int:
    return len(re.findall(r'-[0-9]+', ' '.join(map(str, numbers))))
```
* Using build in functions `str()` and `count()`:
```python
def count_negatives(numbers: list) -> int:
    return str(numbers).count('-')
```


[Task 2](https://gitlab.com/igorshevchuk540/epam_python_course/-/blob/master/Task2/Task2.py)<br>
`Task2.py` swaps first and last element of list. In this task was used slicing.<br>

Function gets list as argument and returns list with swapped first and last element:
```python
def change_positions(players: list):
    players[0], players[-1] = players[-1], players[0]
    print(players)
```


[Task 3](https://gitlab.com/igorshevchuk540/epam_python_course/-/blob/master/Task2/Task3.py)<br>
`Task3.py` swaps two words in a string. In this task was used 2 functions `split()` and `join()`<br>

Function gets string as argument and returns string with swapped two words:
```python
def change_words(string: str):
    s = string.split(' ')
    s[1], s[-11] = s[-11], s[1]
    print(' '.join(s))
```
