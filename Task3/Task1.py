# Solution only for current function
def validate_dot(x1, y1):
    return -1 <= x1 <= 1 and 0 <= y1 <= 1 and y1 >= abs(x1)


if __name__ == '__main__':
    x = float(input('Input x value: '))
    y = float(input('Input y value: '))
    if validate_dot(x, y):
        print('Dot is inside function y = |x|')
    else:
        print('Dot is outside function y = |x|')
