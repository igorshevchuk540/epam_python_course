def fizz_buzz(number: int):
    if num % 5 == 0 and num % 3 == 0:
        print('FizzBuzz')
    elif num % 3 == 0:
        print('Fizz')
    elif num % 5 == 0:
        print('Buzz')
    else:
        print(num)


if __name__ == '__main__':
    num = int(input('Input number: '))
    if num < 0:
        exit(0)
    fizz_buzz(num)
