def baccarat(c1, c2):
    deck = {
        'A': 1,
        '2': 2,
        '3': 3,
        '4': 4,
        '5': 5,
        '6': 6,
        '7': 7,
        '8': 8,
        '9': 9,
        '10': 0,
        'J': 0,
        'Q': 0,
        'K': 0
    }
    try:
        if c1 == 'joker' or c2 == 'joker':
            print('Do not Cheat')
            return 0
        if deck[c1] + deck[c2] > 9:
            print('Your result is: {}'.format(deck[c1] + deck[c2] - 10))
        else:
            print('Your result is: {}'.format(deck[c1] + deck[c2]))
    except KeyError:
        print('There is no suck card in deck')


if __name__ == '__main__':
    card1 = input('Input first card: ')
    card2 = input('Input second card: ')
    baccarat(card1, card2)
