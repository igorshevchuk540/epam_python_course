# Home task for week 3

[Task 1](https://gitlab.com/igorshevchuk540/epam_python_course/-/blob/master/Task3/Task1.py)<br>
`Task1.py` provides a solution to check whether the dot is inside plots y = |x| and y = 1.<br>



[Task 2](https://gitlab.com/igorshevchuk540/epam_python_course/-/blob/master/Task3/Task2.py)<br>
`Task2.py` prints **Fizz** if number can be divided by 3, prints **Buzz** if number can be divided by 5 and prints **FizzBuzz if number can be divided both by 3 and 5**.<br>



[Task 3](https://gitlab.com/igorshevchuk540/epam_python_course/-/blob/master/Task3/Task3.py)<br>
`Task3.py` provides the program to count points in simplified game Baccarat<br>


