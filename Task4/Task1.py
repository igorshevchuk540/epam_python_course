def transpose_matrix(matr):
    transposed = [[matr[j][i] for j in range(len(matr))] for i in range(len(matr[0]))]
    for row in transposed:
        print(row)


if __name__ == '__main__':
    matrix = [[1, 2, 3], [4, 5, 6]]
    transpose_matrix(matrix)
