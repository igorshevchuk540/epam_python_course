def factorial(number: int):
    res = 1
    for i in range(1, number + 1):
        res = res * i
    return res


if __name__ == '__main__':
    num = int(input('Input number: '))
    print(factorial(num))
