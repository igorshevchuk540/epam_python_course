def fibonacci(len: int):
    prev = 0
    ne = 1
    s = 0
    summa = 0
    if len <= 0:
        print("Incorrect input")
    elif len == 1:
        return ne
    else:
        for el in range(len - 1):
            n = prev + ne
            prev = ne
            ne = n
            summa += prev
    return summa


if __name__ == '__main__':
    num = int(input('Input length of Fibonacci sequence: '))
    print(fibonacci(num))
