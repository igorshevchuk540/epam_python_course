def binary_repr(decimal: int):
    binary = []
    while decimal > 0:
        binary.insert(0, str(decimal % 2))
        decimal = decimal // 2
    return ''.join(binary)


def find_sum_binary(binary: str):
    numbers = [int(el) for el in binary]
    return sum(numbers)


if __name__ == '__main__':
    num = int(input('Input number: '))
    print('Number is {}, binary representation is {}, sum is {}'.format(
        num,
        binary_repr(num),
        find_sum_binary(binary_repr(num))
    ))
