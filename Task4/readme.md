# Home task for week 4

[Task 1](https://gitlab.com/igorshevchuk540/epam_python_course/-/blob/master/Task4/Task1.py)<br>
`Task1.py` transposes matrix.<br>



[Task 2](https://gitlab.com/igorshevchuk540/epam_python_course/-/blob/master/Task4/Task2.py)<br>
`Task2.py` gets a number as input and outputs the factorial to this number.<br>



[Task 3](https://gitlab.com/igorshevchuk540/epam_python_course/-/blob/master/Task4/Task3.py)<br>
`Task3.py` gets int number as input and outputs sum of fibonacci numbers to inputed one.<br>


[Task 4](https://gitlab.com/igorshevchuk540/epam_python_course/-/blob/master/Task4/Task4.py)<br>
`Task3.py` representates decimal number in binary way and counts the sum of ones<br>


