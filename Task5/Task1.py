def is_sorted(arr: list, order: str) -> bool:
    if order == 'desc':
        arr = arr[::-1]
    return all(arr[i] <= arr[i + 1] for i in range(len(arr) - 1))


if __name__ == '__main__':
    array = [1, 2, 3, 4, 5, 6, 7, 7, 5]
    print(is_sorted(array, 'asc'))
