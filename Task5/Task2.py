from Task5.Task1 import is_sorted


def transform(arr: list, order: str):
    if is_sorted(arr, order):
        return [el+arr.index(el) for el in arr]
    return arr


if __name__ == '__main__':
    array = [1, 6, 10, 15, 90]
    print(transform(array, 'asc'))
