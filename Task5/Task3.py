def mult_arithmetic_el(a: int, t: int, n: int):
    sum = a
    for i in range(n - 1):
        sum *= a + t
        a += t
    return sum


if __name__ == '__main__':
    print(mult_arithmetic_el(5, 3, 4))
