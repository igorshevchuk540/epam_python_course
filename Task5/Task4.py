def sum_geometric_el(a: int, t: int, elim: float, summ=0):
    while a > elim:
        summ += a
        a = a * t
    return summ


if __name__ == '__main__':
    print(sum_geometric_el(100, 0.5, 20))
