# Home task for week 5

[Task 1](https://gitlab.com/igorshevchuk540/epam_python_course/-/blob/master/Task5/Task1.py)<br>
`Task1.py` checks if array is sorted in given order.<br>



[Task 2](https://gitlab.com/igorshevchuk540/epam_python_course/-/blob/master/Task5/Task2.py)<br>
`Task2.py` sums index and value of elements in array whether it is sorted in given order.<br>



[Task 3](https://gitlab.com/igorshevchuk540/epam_python_course/-/blob/master/Task5/Task3.py)<br>
`Task3.py` multiplies elements of arithmetical progression with given step and amount of elements.<br>


[Task 4](https://gitlab.com/igorshevchuk540/epam_python_course/-/blob/master/Task5/Task4.py)<br>
`Task3.py` returns sum of elements in geometrical progression, than are bigger that elimination value.<br>


