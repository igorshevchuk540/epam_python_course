def combine_dicts(*args):
    returned_dict = dict()
    for el in args:
        for key in el.keys():
            if key in returned_dict.keys():
                returned_dict[key] += el[key]
            else:
                returned_dict[key] = el[key]
    return returned_dict


if __name__ == '__main__':
    dict_1 = {'a': 100, 'b': 200}
    dict_2 = {'a': 200, 'c': 300}
    dict_3 = {'a': 300, 'd': 100}
    print(combine_dicts(dict_1, dict_2))
    print(combine_dicts(dict_1, dict_2, dict_3))
