class CustomList:
    def __init__(self, data=None):
        super(CustomList, self).__init__()
        if data is not None:
            self._list = list(data)
        else:
            self._list = list()

    def __repr__(self):
        return "<{0} {1}>".format(self.__class__.__name__, self._list)

    def __len__(self):
        return len(self._list)

    def __getitem__(self, ii):
        return self._list[ii]

    def __delitem__(self, ii):
        del self._list[ii]

    def __setitem__(self, ii, val):
        self._list[ii] = val

    def __str__(self):
        return str(self._list)

    def insert(self, ii, val):
        self._list.insert(ii, val)

    def index(self, el):
        # return self._list.index(el)
        for i in range(self._list.__len__()):
            if el == self._list[i]:
                return i
        return 'No such element in list'

    def append(self, val):
        self.insert(len(self._list), val)


if __name__ == '__main__':
    custom_list = CustomList([1, '2', {1: 'asd', 2: 11}, 4, 5])
    print(custom_list.__repr__())  # representation of custom list

    custom_list.append(6)  # append item

    custom_list.insert(2, (1,))  # Insert element into custom list

    x = iter(custom_list)  # Iterable
    print(next(x))

    del custom_list[0]  # delete item

    print(custom_list.index(19))
    del custom_list
