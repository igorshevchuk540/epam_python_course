import sys


class Rectangle:
    def __init__(self, a, b=5):
        self._side_a = a
        self._side_b = b

    def get_side_a(self):
        return self._side_a

    def get_side_b(self):
        return self._side_b

    def get_area(self):
        return self._side_a * self._side_b

    def get_perimeter(self):
        return 2 * (self._side_a + self._side_b)

    def is_square(self):
        return self._side_a == self._side_b

    def replace_sides(self):
        self._side_a, self._side_b = self._side_b, self._side_a


class ArrayRectangles:
    __rectangle_array = []
    __length = 0

    def __init__(self, *args):
        # self.__rectangle_array.append(*args)
        if isinstance(args[0], list):
            args = args[0]

        for item in args:
            if isinstance(item, Rectangle):
                self.__rectangle_array.append(item)
                self.__length += 1

    def add_rectangle(self, rectangle: Rectangle):
        self.__rectangle_array.append(rectangle)

    def number_max_area(self):
        _i = 0
        _max = 0
        for rect in self.__rectangle_array:
            if _max < rect.get_area():
                _max = rect.get_area()
                _i = self.__rectangle_array.index(rect)
        return _i

    def number_min_perimeter(self):
        _i = 0
        _min = sys.maxsize
        for rect in self.__rectangle_array:
            if _min > rect.get_perimeter():
                _min = rect.get_perimeter()
                _i = self.__rectangle_array.index(rect)
        return _i

    def number_square(self):
        pass


if __name__ == '__main__':
    rr = Rectangle(5, 4)
    print('Side a: {}'.format(rr.get_side_a()))
    print('Side b: {}'.format(rr.get_side_b()))
    print('Area of the rectangle is: {}'.format(rr.get_area()))
    print('Perimeter of the rectangle is: {}'.format(rr.get_perimeter()))
    print(rr.is_square())
    rr.replace_sides()
    print(rr.__dict__)

    print('##########################################')
    print('##########################################')

    print('Advanced level of homework')
    recs = ArrayRectangles(Rectangle(10, 2), Rectangle(1, 3), Rectangle(2, 4))
    print('Max area index: {}'.format(recs.number_max_area()))
    print('Min perimeter index: {}'.format(recs.number_min_perimeter()))
# (int(self.get_area()**0.5 + 0.5)) ** 2 == self.get_area()
