class Employee:
    _bonus = 0

    def __init__(self, name: str, salary: str):
        self._name = name
        self._salary = salary

    def get_name(self):
        return self._name

    def get_salary(self):
        return self._salary

    def set_bonus(self, bonus):
        self._bonus = bonus

    def to_pay(self):
        return self._salary + self._bonus


class SalesPerson(Employee):
    def __init__(self, name: str, salary: str, percent: int):
        super().__init__(name, salary)
        self._percent = percent

    def set_bonus(self, bonus):
        if self._percent >= 200:
            self._bonus = bonus * 3
        elif self._percent >= 100:
            self._bonus = bonus * 2
        else:
            self._bonus = bonus


if __name__ == '__main__':
    jim = SalesPerson('Jim', 5000, 90)
    jim.set_bonus(500)
    print(jim.__dict__)

    dwight = SalesPerson('Dwight', 5000, 150)
    dwight.set_bonus(500)
    print(dwight.__dict__)

    michael = Employee('Michael', 10000)
    michael.set_bonus(1000)
    print(michael.__dict__)
