# Home task for week 8

Take a look at code and tell the output<br>
Code for `Task 1`:<br>
```python
a = int(input('Enter the number: '))
b = 7
assert a > b, 'Not enough'
```
If `a` is bigger, than 7, than everything will be ok, but when `a` is less, than 7, then code will give AssertionError.

<br>
<br>

Take a look at code and tell the output<br>
Code for `Task 2`:<br>
```python
def foo():
    try:
        bar(x, 4)
    finally:
        print('This after bar')
    print('Or this after bar')
foo()
```
In my opinion, the code will run, it will print both `'This after bar'` and `'Or this after bar'`.

<br>
<br>

Take a look at code and tell the output<br>
Code for `Task3`:<br>
```python
def bax():
try:
        return 1
        with open('/tmp/logs.txt') as file:
            print(file.read())
            return
finally:
        return 2

retusl = bax()
print(result)
```
I  think, that the code will be executed. File will be opened, and it`s content will be displayed. After that will execute return section.

<br>
<br>

Take a look at code and tell the output<br>
Code for `Task 4`:<br>
```python
def foo():
    try:
        print(1)
        return 
    finally:
        print(2)
    else:
        print(3)

foo()
```
I think, that code will crush, because `finally` must be after `else`, and `else` need `except` section before it.

<br>
<br>

Take a look at code and tell the output<br>
Code for Task 5:<br>
```python
try:
    if '1' != 1:
        raise 'Error'
    else:
        print('Error has not occuded')
except 'Error'
    print('Error has occured')
```
I think, that `'Error'` will be raised and also message `'Error has occured'` will be printed.

<br>
<br>

Take a look at code and tell the output<br>
Code for Task 6:<br>
```python
flag = False
while not flag:
    try:
        filename = input('Enter filename: ')
        file = open(filename, 'r')
    except:
        print('Input file not found')
```
2 possible versions while running this code:
1. If file name is properly written, than file opens, and no exception occurs.
2. If file name is wrond, that exception will be raised, and message `'Input file not found'` `printed.
