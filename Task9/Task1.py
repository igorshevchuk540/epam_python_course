def replace_symbols(st, s=''):
    # s = list(st)
    # for i in range(len(s)):
    #     if s[i] == '\'':
    #         s[i] = '"'
    #         continue
    #     if s[i] == '"':
    #         s[i] = '\''
    #         continue
    # print(s)
    # print(st)
    # print(''.join(s))
    for el in st:
        if el == "'":
            el = '\"'
        elif el == '\"':
            el = "'"
        s += el
    return s


if __name__ == '__main__':
    string = 'Igor\'s strin\"g'
    print(replace_symbols(string))
