def is_palindrome(st):
    st.replace(' ', '')
    s = ''
    for char in st:
        if char in ['"', "'", '?', '!', ',', '.', ' ']:
            continue
        else:
            s += char.lower()
    return s[::] == s[::-1]


if __name__ == '__main__':
    string = 'Do geese see God'
    print(is_palindrome(string))
