def get_shortest_word(s: str) -> str:
    min_word = ''
    min_length = 9999999
    word = ''
    length = 0
    for i in range(len(s)):
        if s[i] == ' ':
            if length < min_length:
                min_word = word
                min_length = length
            word = ''
            length = 0
            continue
        elif i == len(s) - 1:
            word += s[i]
            length += 1
            if length < min_length:
                return word
        word += s[i]
        length += 1

    return min_word


if __name__ == '__main__':
    string = 'Process finished with exit code bbb'
    print(get_shortest_word(string))
