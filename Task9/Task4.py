def get_dict_of_elements(s: list):
    n = []
    for el in s:
        n += set(el)
    n = ''.join(n)
    d = {}
    for el in n:
        if el not in d.keys():
            d[el] = 1
        else:
            d[el] += 1
    return d


def char_in_all_strings(s: list):
    """Contains 2 ways"""
    # 1st
    return set(s[0]).intersection(*s)

    # 2nd
    # d = get_dict_of_elements(s)
    # ans = set()
    # for key in d:
    #     if d[key] == len(s):
    #         ans.add(key)
    # return ans


def unique_elements(s: list):
    return set(''.join(s))


def char_in_2_strings(s: list):
    d = get_dict_of_elements(s)
    elements = set()
    for key in d:
        if d[key] >= 2:
            elements.add(key)
    return elements


def chars_not_appear(s: list):
    alphabet = set(map(chr, range(97, 123)))
    return alphabet - unique_elements(s)


if __name__ == '__main__':
    strings = ['hello', 'world', 'python']
    print(char_in_all_strings(strings))
    print(unique_elements(strings))
    print(char_in_2_strings(strings))
    print(chars_not_appear(strings))
    l = list(chars_not_appear(strings)) + list(unique_elements(strings))
    l.sort()
    print(l)
