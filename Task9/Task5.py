def dict_counter(s: str):
    d = {}
    for el in s:
        if el not in d.keys():
            d[el] = 1
        else:
            d[el] += 1
    return d


if __name__ == '__main__':
    string = 'Process finished with exit code bbb'
    print(dict_counter(string))
