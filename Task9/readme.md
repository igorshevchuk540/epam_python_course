# Home task for week 9

[Task 1](https://gitlab.com/igorshevchuk540/epam_python_course/-/blob/master/Task9/Task1.py)<br>
`Task1.py` replaces single quotes into double and vise versa.<br>



[Task 2](https://gitlab.com/igorshevchuk540/epam_python_course/-/blob/master/Task9/Task2.py)<br>
`Task2.py` checks if the string is palindrome.<br>



[Task 3](https://gitlab.com/igorshevchuk540/epam_python_course/-/blob/master/Task9/Task3.py)<br>
`Task3.py` returns shortest word in string<br>



[Task 4](https://gitlab.com/igorshevchuk540/epam_python_course/-/blob/master/Task9/Task4.py)<br>
`Task2.py` consists of 4 tasts:<br>
1. characters that appear in all strings
2. characters that appear in at least one string
3. characters that appear at least in two strings
4. characters of alphabet, that were not used in any stringNote: use string.ascii_lowercase for list of alphabet letters



[Task 5](https://gitlab.com/igorshevchuk540/epam_python_course/-/blob/master/Task9/Task5.py)<br>
`Task2.py` returns amount of every symbol in text.<br>

